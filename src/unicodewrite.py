'''
THis program is attributed to Dr Piyush Kumar from FSU
THis program is used to get a better understanding of unicode
'''

with open("unicodetest",'wb') as f:
    str = '\u0100\u0110\u0120\u0130\u0140'
    f.write(str.encode('utf-8'))


with open("unicodetest",'rb') as f:
    unibytes = f.read()
print(unibytes)
print(type(unibytes))
newstr = unibytes.decode('utf-8')
print(newstr)
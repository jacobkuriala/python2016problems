'''
This is a basic version of sorting using merge sort algorithm
'''

import cProfile
from multiprocessing import Pool


def multiprocessmergesort(start, end, arr):
    arr3=[]
    if end-start > 1:
        midpoint = start + ((end-start)//2)

        with Pool(2) as p:
            arrs = p.starmap(mergesort, [(start,midpoint,arr),(midpoint,end,arr)])

        arr3 = merge(arrs[0],arrs[1])
    elif end-start == 1:
        return arr[int(start):int(start+1)]
    return arr3


def mergesort(start, end, arr):
    arr3=[]
    if end-start > 1:
        midpoint = start + ((end-start)//2)
        arr1 = mergesort(start,midpoint,arr)
        arr2 = mergesort(midpoint,end,arr)
        arr3 = merge(arr1,arr2)
    elif end-start == 1:
        return arr[int(start):int(start+1)]
    return arr3


def merge(arr1,arr2):
    counter1, counter2 = 0, 0
    a = []
    while len(a) < len(arr1)+len(arr2):
        if counter1 == len(arr1):
            a.extend(arr2[counter2:])
            break
        elif counter2 == len(arr2):
            a.extend(arr1[counter1:])
            break
        elif arr1[counter1]>arr2[counter2]:
            a.append(arr1[counter1])
            counter1 += 1
        else:
            a.append(arr2[counter2])
            counter2 += 1
    return a

if __name__ == '__main__':
    import random

    #alist = [54, 26, 93, 17, 77, 31, 44, 55, 20,30,31,32,1,2,3,4,3,2,1]

    alist = random.sample(range(0,10000000),1000000)
    print('List generated')
    cProfile.run('multiprocessmergesort(0,len(alist),alist)')
    #newlist = mergesort(0,len(alist),alist)
    #print(newlist)
'''
Take string
Converts to lower case
remove spaces
make all lowercase
get frequency of trigrams

try using counter from collections
'''
def trigramfrequency(mystr):
    lowerinputstring = mystr.lower()
    strwithoutspace = ''.join(lowerinputstring.split(' '))

    d = {}
    for i in range(0, len(strwithoutspace)-2):
        trigram = strwithoutspace[i:i+3]
        d[trigram] = d.get(trigram , 0) + 1
    return d

def test_threegram():
    s = "abcabc"
    d = {'abc':2,'bca':1,'cab':1}
    assert d == trigramfrequency(s)

'''
inputstr = input("Enter input string")
d = trigramfrequency(inputstr)
print(d)
'''

test_threegram()

'''
Write a program to print whether an input is prime or not
'''

import math

if __name__ == "__main__":
    inputnumber = input("Enter the number to check primality:")

    notprime = False
    for i in range(2,int(math.sqrt(inputnumber))+2):
        if inputnumber%i ==0:
            notprime=True
            print ("Number is not prime")
            break
    if(not notprime):
        print("Number is prime")


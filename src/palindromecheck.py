''''
Pybooks.rocks
Check if a string is a palindrome or not.
'''

if __name__ == "__main__":
    strPalindrome = "abcba"
    lefthalf = strPalindrome[0:int(len(strPalindrome) / 2)]

    endloc = int(len(strPalindrome) / 2)
    startloc = int(len(strPalindrome)) - 1

    if len(strPalindrome) % 2 == 0:
        endloc = endloc-1

    righthalfreversed = strPalindrome[startloc:endloc:-1]

    if(lefthalf == righthalfreversed):
        print('Palindrome')
    else:
        print('Not Palindrome')

    #print(list(strPalindrome))

'''
Write a python code to print Armstrong number in a given interval.
An Armstrong number is a number that is equal to the sum of the
cubes of its own digits. For example 371 is an Armstrong number
as 371 = 3*3*3 + 7*7*7 +1*1*1.
The user will have to enter an interval and the program should display
all the Armstrong numbers within that interval.
The program should terminate when user gives the range as 0.
'''
if __name__ == "__main__":

   while True:
       originalnumber = int(input("Please enter a number. (0 to exit): "))
       if originalnumber == 0:
           break

       armtestno = 0
       numberless1digit = originalnumber

       while(numberless1digit!=0):
           remainder = numberless1digit % 10
           numberless1digit =  numberless1digit // 10
           armtestno += remainder*remainder*remainder

       if originalnumber == armtestno:
           print(originalnumber, " is an armstrong number.")
       else:
           print(originalnumber, " is not an armstrong number." ,armtestno)






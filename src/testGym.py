'''
dependencies:
sudo apt install swig # this is required to build and install gym

http://askubuntu.com/questions/575505/glibcxx-3-4-20-not-found-how-to-fix-this-error
sudo apt-get install libstdc++6

sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade

'''

import gym

env = gym.make('MsPacman-v0')
env.reset()

for _ in range(1000):
    env.render()
    env.step(env.action_space.sample()) # take a random action
